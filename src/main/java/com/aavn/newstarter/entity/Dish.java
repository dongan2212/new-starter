package com.aavn.newstarter.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Dish {

    @Id
    @Column(name = "dishId")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long dishId;

    @Column(name = "dishName", nullable = false)
    private String dishName;

    @Column(name = "price", nullable = false)
    private Integer price;

    @Column(name = "averageRating")
    private Double averageRating;

    @Column(name = "dishImageUrls", length = 2000)
    private String dishImageUrl;

    @OneToMany(mappedBy = "dish")
    private Set<DishComment> dishComments;

    @ManyToOne
    @JoinColumn(name = "restaurantId")
    private Restaurant restaurant;

    public Long getDishId() {
        return dishId;
    }

    public void setDishId(Long dishId) {
        this.dishId = dishId;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }

    public Set<DishComment> getDishComments() {
        return dishComments;
    }

    public void setDishComments(Set<DishComment> dishComments) {
        this.dishComments = dishComments;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public String getDishImageUrl() {
        return dishImageUrl;
    }

    public void setDishImageUrl(String dishImageUrl) {
        this.dishImageUrl = dishImageUrl;
    }
}
