package com.aavn.newstarter.entity;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.sql.Timestamp;
import java.util.Set;

@Entity
public class Restaurant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "restaurantId")
    private Long restaurantId;

    @Column(name = "name", nullable = false)
    private String restaurantName;

    @Column(name = "restaurantImageUrls", length = 2000, nullable = false)
    private String restaurantImageUrl;

    @Column(name = "contactNumber")
    @Pattern(regexp = "(^$|[0-9]{9,12})", message = "PHONE_INVALID")
    private String contactNumber;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "openingTime")
    private String openingTime;

    @Column(name = "closedTime")
    private String closedTime;

    @Column(name = "shortDescription", length = 300)
    private String shortDescription;

    @Column(name = "publishedDate", nullable = false)
    private Timestamp publishedDate;

    @Column(name = "averageRating")
    private Double averageRating;

    @OneToMany(mappedBy = "restaurant")
    private Set<RestaurantComment> restaurantComments;

    @OneToMany(mappedBy = "restaurant")
    private Set<Announcement> announcements;

    @OneToMany(mappedBy = "restaurant")
    private Set<Dish> dishes;

    public Long getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(Long restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantImageUrl() {
        return restaurantImageUrl;
    }

    public void setRestaurantImageUrl(String restaurantImageUrl) {
        this.restaurantImageUrl = restaurantImageUrl;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public String getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(String openingTime) {
        this.openingTime = openingTime;
    }

    public String getClosedTime() {
        return closedTime;
    }

    public void setClosedTime(String closedTime) {
        this.closedTime = closedTime;
    }

    public Set<RestaurantComment> getRestaurantComments() {
        return restaurantComments;
    }

    public void setRestaurantComments(Set<RestaurantComment> restaurantComments) {
        this.restaurantComments = restaurantComments;
    }

    public Set<Announcement> getAnnouncements() {
        return announcements;
    }

    public void setAnnouncements(Set<Announcement> announcements) {
        this.announcements = announcements;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public Set<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(Set<Dish> dishes) {
        this.dishes = dishes;
    }

    public Timestamp getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Timestamp publishedDate) {
        this.publishedDate = publishedDate;
    }

    public Double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }


    @Override
    public String toString() {
        return super.toString();
    }
}
