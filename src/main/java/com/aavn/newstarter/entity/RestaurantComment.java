package com.aavn.newstarter.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
public class RestaurantComment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long restaurantCommentId;

    @Column(name = "content")
    @Size(min = 3, max = 100, message = "Comment need to be between 3 and 100 characters")
    private String content;

    @ManyToOne
    @JoinColumn(name = "restaurantId")
    private Restaurant restaurant;

    @ManyToOne
    @JoinColumn(name = "accountId")
    private Account account;

    @Column(name = "rating")
    private Integer rating;

    public Long getRestaurantCommentId() {
        return restaurantCommentId;
    }

    public void setRestaurantCommentId(Long restaurantCommentId) {
        this.restaurantCommentId = restaurantCommentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
