package com.aavn.newstarter.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
public class DishComment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long dishCommentId;

    @Column(name = "content")
    @Size(min = 3, max = 100, message = "Comment need to be between 3 and 100 characters")
    private String content;

    @ManyToOne
    @JoinColumn(name = "dishId")
    private Dish dish;

    @ManyToOne
    @JoinColumn(name = "accountId")
    private Account account;

    @Column(name = "rating")
    private Integer rating;

    public Long getDishCommentId() {
        return dishCommentId;
    }

    public void setDishCommentId(Long dishCommentId) {
        this.dishCommentId = dishCommentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Dish getDish() {
        return dish;
    }

    public void setDish(Dish dish) {
        this.dish = dish;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
